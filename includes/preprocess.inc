<?php

/**
 * @file
 * Set up variables to be placed within the template (.html.twig) files.
 *
 * The variables set up here apply to both templates (.html.twig) files and
 * functions (theme_HOOK). These are also used for providing
 * @link https://www.drupal.org/node/2354645 Twig Template naming conventions @endlink.
 *
 * @see process.inc
 */

use Drupal\Component\Utility\Html;
use Drupal\block\Entity\Block;

/**
 * Implements template_preprocess_page().
 */
function ukstyle_preprocess_page(&$variables) {
  // if (isset($variables['sidebar_second_attributes']) && $variables['sidebar_second_attributes']->hasClass('uk-width-1-4@l')) {
  //   $variables['sidebar_second_attributes']->removeClass('uk-width-1-4@l');
  //   $variables['sidebar_second_attributes']->addClass('uk-width-1-3@l');
  //   $variables['content_attributes']['class'] = array_replace($variables['content_attributes']['class'], array('uk-width-expand@l'));
  // }
}

/**
 * Implements template_preprocess_block().
 */

function ukstyle_preprocess_block(&$variables) {
  // Provide an id attribute to help themers.
  if (isset($variables['elements']['#id']) && $id = $variables['elements']['#id']) {
    $id = Html::cleanCssIdentifier($id);
    $variables['attributes']['id'] = $id;

    $base_plugin_id = $variables['base_plugin_id'];
    $system_menu_block = $base_plugin_id == 'system_menu_block';
    if ($block = Block::load($variables['elements']['#id'])) {
      $region = $block->getRegion();

      switch ($region) {
        case 'account_offcanvas':
          if ($system_menu_block) {
            // Define #theme variable for offcanvas menus.
            $variables['content']['#theme'] = 'menu__offcanvas';
          }
          break;

      }
    }
  }
}

/**
 * Implements template_preprocess_breadcrumb().
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_breadcrumb(&$variables) {
}
// */

/**
 * Implements template_preprocess_details().
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_details(&$variables) {
}
// */

/**
 * Implements hook_preprocess_HOOK() for feed-icon.html.twig.
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_feed_icon(&$variables) {
}
// */

/**
 * Implements template_preprocess_form_element().
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_form_element(&$variables) {
}
// */

/**
 * Implements template_preprocess_form_element() for form-element--advanced.html.twig.
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_form_element__advanced(&$variables) {
}
// */

/**
 * Implements template_preprocess_links() for links--dropbutton.html.twig.
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_links__dropbutton(&$variables) {
}
// */

/**
 * Implements template_preprocess_menu_local_action().
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_menu_local_action(&$variables) {
}
// */

/**
 * Implements hook_preprocess_HOOK() for menu-local-tasks.html.twig.
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_menu_local_tasks(&$variables) {
}
// */

/**
 * Implements hook_preprocess_HOOK() for menu--navbar.html.twig
 */
// function ukstyle_preprocess_menu__navbar(&$variables) {
// }

/**
 * Implements hook_preprocess_HOOK() for menu--offcanvas.html.twig
 */
/* -- Delete this line if you want to use this function
function ukstyle_preprocess_menu__offcanvas(&$variables) {
}
// */

/**
 * Implements hook_preprocess_HOOK() for menu.html.twig
 */
function ukstyle_preprocess_menu(&$variables) {
  _ukstyle_set_menu_icon($variables, 'main', 'standard.front_page', 'home');
  _ukstyle_set_menu_icon($variables, 'main', 'starterkit.front_page', 'home');
  _ukstyle_set_menu_icon($variables, 'account', 'user.page', 'user');
  if (\Drupal::currentUser()->isAnonymous()) {
    // Anonymous user...
    _ukstyle_set_menu_icon($variables, 'account', 'user.logout', 'sign-in');
  } else {
    _ukstyle_set_menu_icon($variables, 'account', 'user.logout', 'sign-out');
  }
}
